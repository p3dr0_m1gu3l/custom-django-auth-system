from django.conf.urls import url

from hello.views import HomePageView, AboutUsPageView


urlpatterns = [
    url(r"^$", HomePageView.as_view(), name="index"),
    url(r"^about-us/$", AboutUsPageView.as_view(), name="about_us"),
]
