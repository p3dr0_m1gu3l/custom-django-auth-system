from django.contrib.auth.mixins import LoginRequiredMixin
from django.views.generic.base import TemplateView


class HomePageView(LoginRequiredMixin, TemplateView):
    template_name = 'hello/index.html'


class AboutUsPageView(LoginRequiredMixin, TemplateView):
    template_name = 'hello/about.html'
