from django.conf.urls import url
from django.contrib.auth import views as auth_views

from . import views


urlpatterns = [
    url(r"^signin/$", views.signin, name="signin"),
    url(r"^password_reset/$",
        auth_views.password_reset,
        {"template_name": "accounts/password_reset.html",
         "email_template_name": "accounts/password_reset_email.html",
         "subject_template_name": "accounts/password_reset_subject.txt",
         "post_reset_redirect": "password_reset/done/"},
        name="password_reset"),
    url(r"^password_reset/done/$",
        auth_views.password_reset_done,
        {"template_name": "accounts/password_reset_done.html"},
        name='password_reset_done'),
    url(r"^reset/(?P<uidb36>[0-9A-Za-z]+)-(?P<token>.+)/$",
        auth_views.password_reset_confirm,
        {"template_name": "accounts/password_reset_confirm.html"})
]
