from django.shortcuts import redirect
from django.contrib.auth.views import login as django_login

from django.conf import settings


def signin(request):
    if request.user.is_authenticated():
        return redirect(settings.LOGIN_REDIRECT_URL)
    return django_login(request, template_name='accounts/signin.html')
